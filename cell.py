import pygame

from consts import *

class Cell:
    """CONSTRUCTOR"""
    def __init__(self, row, col):
        # Positions
        self.grid_pos = (row, col)
        self.screen_pos = (col * CELL_SZ, row * CELL_SZ)

        # Logique / gameplay
        self.is_bomb = False
        self.bombs_around = 0

        self.is_discovered = False
        self.has_flag = False

        self.exploded = False

        # Affichage
        self.rect_surf = pygame.Rect(self.screen_pos, (CELL_SZ, CELL_SZ))
        self.color = GREY_COLOR
        self.txt = ""

    
    """METHODS"""
    def count_around(self, grid):
        count = 0

        for i in range(self.grid_pos[0] - 1, self.grid_pos[0] + 2): # row + 1 included
            for j in range(self.grid_pos[1] - 1, self.grid_pos[1] + 2): # col + 1 included
                if i < 0 or i > 8 or j < 0 or j > 8: # en dehors de la grille
                    continue
                else:
                    if grid[i][j].is_bomb:
                        count += 1

        self.bombs_around = count


    def dig(self):
        self.is_discovered = True
        
        # Si c'est une bombe
        if self.is_bomb:
            self.explode() # elle explose

        # Si il y a un drapeau
        if self.has_flag:
            self.has_flag = False # on l'enlève


    def explode(self):
        self.exploded = True
        self.is_discovered = True


    """UPDATE & DRAW"""
    def update(self, grid, game_over):
        self.count_around(grid)

        if game_over == GAME_LOST:
            # On ne découvre QUE les bombes qui n'ont pas de drapeau
            if self.is_bomb and not self.has_flag:
                self.is_discovered = True

        # Couleur
        self.txt_color = (0, 0, 0)
        
        #Si on explose une bombe
        if self.exploded:
            self.color = RED_COLOR
        #Si elle n'est pas explosée
        elif self.is_discovered:
            self.color = WHITE_COLOR

        # Texte
        #Si découverte
        if self.is_discovered:
            #Si bombe
            if self.is_bomb:
                self.txt = "B"
            else:
                # Ne rien afficher si aucun drapeau
                if not self.has_flag:
                    self.txt = ""

                # Afficher le nombre de bombes autour si != 0
                if self.bombs_around != 0:
                    self.txt = str(self.bombs_around)
                    self.txt_color = CELL_TXT_COLORS[self.bombs_around - 1]
                
        #Si non découverte
        else:
            #Si drapeau
            if self.has_flag:
                #Si partie en cours
                if game_over == GAME_RUNNING:
                    self.txt = "F"
                #Sinon
                else:
                    #Si drapeau et bombe
                    if self.is_bomb:
                        self.txt = "F"
                    else:
                        self.txt = "X" 
            #Si pas de drapeau
            else:
                self.txt = ""


    def draw(self, surface, font):
        pygame.draw.rect(surface, self.color, self.rect_surf)
        pygame.draw.rect(surface, (0, 0, 0), self.rect_surf, 1)

        # Affichage texte
        txt_surf = font.render(self.txt, True, self.txt_color)

        # Position
        txt_size = font.size(self.txt)

        # Offset
        txt_off = ((CELL_SZ - txt_size[0]) / 2, (CELL_SZ - txt_size[1]) / 2)
        txt_pos = (self.screen_pos[0] + txt_off[0], self.screen_pos[1] + txt_off[1]) # (x + x, y + y)

        surface.blit(txt_surf, txt_pos)
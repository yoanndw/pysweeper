# WINDOW #
WIN_TITLE = "PySweeper"
WIN_RES = (900, 600)

# GAME #
GAME_LOST = -1
GAME_RUNNING = 0
GAME_WON = 1

GEY_VALUE = 150
GREY_COLOR = (GEY_VALUE, GEY_VALUE, GEY_VALUE)

WHITE_VALUE = 240
WHITE_COLOR = (WHITE_VALUE, WHITE_VALUE, WHITE_VALUE)

RED_COLOR = (255, 0, 0)

FONT_SZ = 26
TXT_FONT_SZ = 18

CELL_SZ = 50

BOMBS_NB = 10

# CELL'S TXT COLORS #
CELL_TXT_COLORS = [
    (0,   0,   255), # 1
    (0,   128,   0), # 2
    (255, 0,     0), # 3
    (0,   0,   128), # 4
    (128, 0,     0), # 5
    (0,   128, 128), # 6
    (0,   0,     0), # 7
    (128, 128, 128), # 8
]

# CTRLS #
MOUSE_LEFT = 1
MOUSE_RIGHT = 3

# INFORMATIONS #
HELP_TXTS = [
    " ",
    "Right click to place a flag",
    "Left click to dig the cell",
    " ",
    "Symbols :",
    "    F: there is a flag (and a bomb (if you lost))",
    "    B: there was a bomb (only when you lost)",
    "    X: there was a flag but no bomb (only when you lost)",
    " ",
    "R to replay"
]
import random as rnd
from math import floor

import pygame

from consts import *
import cell

"""----------------GRID----------------"""
def screen_to_grid(screen_pos):
    return (floor(screen_pos[1] / CELL_SZ), floor(screen_pos[0] / CELL_SZ))

def between(val, min, max):
    return val >= min and val <= max

def in_grid_screen(pos):
    return between(pos[0], 0, CELL_SZ * 9) and between(pos[1], 0, CELL_SZ * 9)

def in_grid(pos):
    return between(pos[0], 0, 8) and between(pos[1], 0, 8)

def get_cell(grid, grid_pos):
    return grid[grid_pos[0]][grid_pos[1]]

"""------------------------------------"""

def place_bomb(grid, bombs_lst): # place UNE bombe
    # On choisit des coords randoms
    row = rnd.randint(0, 8)
    col = rnd.randint(0, 8)

    while grid[row][col].is_bomb == True: # s'il y a une bombe
        # On choisit d'autre coords
        row = rnd.randint(0, 8)
        col = rnd.randint(0, 8)

    grid[row][col].is_bomb = True
    bombs_lst.append(grid[row][col]) # on rajoute la cellule dans notre liste de bombes


def update_grid(grid, game_over):
    for i in range(9):
        for j in range(9):
            c = grid[i][j]
            c.update(grid, game_over)


def dig_cell(grid, grid_pos):
    row, col = grid_pos[0], grid_pos[1]

    cell = grid[row][col]
    cell.dig()

    if cell.exploded: # si la bombe a explosée (parce que c'était une bombe)
        lose()


def is_bomb(grid, grid_pos):
    return grid[grid_pos[0]][grid_pos[1]].is_bomb

# Digging "useless" cells #
def empty_cell(grid, grid_pos):
    if grid_pos[0] < 0 or grid_pos[0] > 8 or grid_pos[1] < 0 or grid_pos[1] > 8:
        return False
    
    if get_cell(grid, grid_pos).bombs_around == 0:
        return True


def dig_cells(grid, grid_pos, digged_cells_lst):
    """
    - On met dans la pile d'appel toutes les cases à creuser
    - Lorsqu'on remonte cette pile (on "dépile"), on creuse ces cases
    """

    cell = get_cell(grid, grid_pos)

    if not cell in digged_cells_lst:
        digged_cells_lst.append(cell)
    else:
        return

    if cell.bombs_around != 0:
        dig_cell(grid, grid_pos)
        return
    else:
        # Creuse les 8 cases autour...
        for i in range(grid_pos[0] - 1, grid_pos[0] + 2): # lignes grid_pos[0] + 1 inclus
            for j in range(grid_pos[1] - 1, grid_pos[1] + 2): # colonnes grid_pos[1] + 1 inclus
                if in_grid((i, j)): # si case autour dans la grille
                    # ...récursivement
                    dig_cells(grid, (i, j), digged_cells_lst)

                    dig_cell(grid, grid_pos)


def toggle_flag(grid, grid_pos, bombs_left):
    global _bombs_left
    _bombs_left = bombs_left

    row, col = grid_pos[0], grid_pos[1]
    c = grid[row][col]

    c.has_flag = not c.has_flag

    # On vient de changer l'état de has_flag
    if c.has_flag: # si maintenant il a un drapeau
        _bombs_left -= 1
    else: # si maintenant il n'en a pas
        _bombs_left += 1


def draw_grid(surf, grid, font):
    # Affichage grille
    for i in range(9):
        for j in range(9):
            c = grid[i][j]
            c.draw(surf, font)


def lose():
    global _game_over
    _game_over = GAME_LOST

def draw_bombs_left(__window_surf, bombs_left, font, yoff):
    bl_txt = "Bombs left : " + str(bombs_left)

    bl_surf = font.render(bl_txt, True, (255, 255, 255))

    x, y = 9 * CELL_SZ, yoff
    __window_surf.blit(bl_surf, (x, y))


def draw_game_state(__window_surf, game_state, font, yoff):
    game_st_txt = ""
    if game_state == GAME_RUNNING:
        game_st_txt = "You're playing"
    elif game_state == GAME_LOST:
        game_st_txt = "You lost :("
    else:
        game_st_txt = "You won !"

    game_st_txt_surf = font.render(game_st_txt, True, (255, 255, 255))

    x, y = 9 * CELL_SZ, yoff

    __window_surf.blit(game_st_txt_surf, (x, y))


def draw_help(__window_surf, font, yoff):
    help_txt_surf = []
    for txt in HELP_TXTS:
        s = font.render(txt, True, (255, 255, 255))
        help_txt_surf.append(s)

    x, y = 9 * CELL_SZ, yoff
    for surf in help_txt_surf:
        __window_surf.blit(surf, (x, y))
        y += surf.get_height()


def draw_HUD(__window_surf, bombs_left, game_state, font):
    draw_bombs_left(__window_surf, bombs_left, font, 0)
    draw_game_state(__window_surf, game_state, font, 20)
    draw_help(__window_surf, font, 40)

"""-------------------------MAIN CODE-------------------------"""

_game_over = None

_grid = []
_bombs_lst = []
_digged = []

_mouse_pos = (0, 0)

_bombs_left = BOMBS_NB

_font = None
_txt_font = None

def load_content():
    global _font
    _font = pygame.font.SysFont("arial", FONT_SZ, True) # bold = True

    global _txt_font
    _txt_font = pygame.font.SysFont("arial", TXT_FONT_SZ)


def init():
    global _bombs_left
    _bombs_left = BOMBS_NB

    global _game_over
    _game_over = GAME_RUNNING

    _digged.clear() # on vide la liste des cases creusées

    _grid.clear()

    # Création grille
    for i in range(9):
        _grid.append([cell.Cell(i, j) for j in range(9)]) # on fonctionne comme une liste et non un tableau

    # Place les bombes
    for b in range(BOMBS_NB):
        place_bomb(_grid, _bombs_lst)


def update(__events):
    global _game_over
    global _bombs_left

    # Updating mouse position
    _mouse_pos = pygame.mouse.get_pos()

    for e in __events:
        # Key pressing
        if e.type == pygame.KEYDOWN:
            # R pour rejouer
            if e.key == pygame.K_r:
                init()
            # Debug
            elif e.key == pygame.K_d:
                print(len(_digged))

        # Mouse clicking
        if e.type == pygame.MOUSEBUTTONDOWN:
            # Si le jeu n'est pas fini
            if not _game_over:
                # Si la souris est dans la grille
                if in_grid_screen(_mouse_pos):
                    grid_pos = screen_to_grid(_mouse_pos)
                    cell = get_cell(_grid, grid_pos)
                    # Clic gauche: on creuse si pas de drapeau
                    if e.button == MOUSE_LEFT:
                        if not cell.has_flag:
                            dig_cells(_grid, grid_pos, _digged)
                    # Clic droit: on place un drapeau si non creusée
                    elif e.button == MOUSE_RIGHT:
                        if not cell.is_discovered: # si la case n'est pas découverte
                            toggle_flag(_grid, grid_pos, _bombs_left) # on peut placer et enlever le drapeau


    update_grid(_grid, _game_over)

    if len(_digged) == 9 * 9 - BOMBS_NB: # cases totales - bombes
        _game_over = GAME_WON
        _bombs_left = 0


def draw(__window_surf):
    draw_grid(__window_surf, _grid, _font)

    global _game_over
    global _bombs_left
    draw_HUD(__window_surf, _bombs_left, _game_over, _txt_font)
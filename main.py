#coding:utf-8

import pygame
import game

from consts import *

pygame.init()

_window_surface = pygame.display.set_mode(WIN_RES)
pygame.display.set_caption(WIN_TITLE)

_game_running = True

game.load_content()
game.init()

while _game_running:
    events = pygame.event.get()

    for event in events:
        if event.type == pygame.QUIT:
            _game_running = False

    game.update(events)

    # Clear screen each frame
    _window_surface.fill((0, 0, 0, 0))
    game.draw(_window_surface)

    pygame.display.flip()


pygame.quit()